$(document).ready(function() {

	$('.slider .container').slick({
		slidesToShow: 1,
	  	slidesToScroll: 1,
	  	autoplay: true,
	  	autoplaySpeed: 4000,
	  	speed: 500,
	  	dots: true
	});
});